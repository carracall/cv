@default_files = ('main.tex');
$pdf_mode = 1;
$pdflatex = 'lualatex -shell-escape -synctex=1 -interaction=nonstopmode';
@generated_exts = (@generated_exts, 'synctex.gz');
